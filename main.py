import nblearn3
import nbclassify3

stopword_list = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself',
                 'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself',
                 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that',
                 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had',
                 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as',
                 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through',
                 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off',
                 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how',
                 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not',
                 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should',
                 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', 'couldn', 'didn', 'doesn', 'hadn', 'hasn',
                 'haven', 'isn', 'ma', 'mightn', 'mustn', 'needn', 'shan', 'shouldn', 'wasn', 'weren', 'won', 'wouldn',
                 'id', 'im', 'ive', 'couldnt', 'itd', 'made', 'didnt', 'could', 'came', 'get']
exclude = {';', '~', '+', "'", ',', '.', '%', '_', '[', '{', '`', '<', '\\', '=', '*', '!', '/', '}', '"', ':', '@',
           ')', ']', '$', '?', '(', '#', '-', '|', '&', '>', '^'}

out_file = 'IO Files/nboutput.txt'
model_filename = 'IO Files/nbmodel.txt'
labelData = 'IO Files/train-labels.txt'
trainData = 'IO Files/train-text.txt'
testData = 'IO Files/test-text.txt'

if __name__ == "__main__":

    """Learning"""
    review_list = nblearn3.read_data(labelData)
    training_token_list = nblearn3.cleaning(review_list, exclude, stopword_list)
    classifier1_labels, classifier2_labels = nblearn3.create_label_lists(labelData)
    prior = nblearn3.calculate_priors(classifier1_labels, classifier2_labels)
    word_probability_dict = nblearn3.calculate_probabilities(training_token_list, classifier1_labels, classifier2_labels)
    nblearn3.write_to_file(word_probability_dict, prior, model_filename)

    """Classification"""
    word_probabilities, priors = nbclassify3.read_model(model_filename)
    character_seq, list_fin = nbclassify3.read_test_file(testData)
    final_list = nblearn3.cleaning(list_fin, exclude, stopword_list)
    review_classification = nbclassify3.compute_probabilities(word_probabilities, priors, final_list)
    nbclassify3.write_to_file(review_classification, out_file, character_seq)