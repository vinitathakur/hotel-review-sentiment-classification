##Hotel Review Sentiment Classification using Naive Bayes Classifier
 
###Introduction
* Includes a Naive Bayes classifier to identify hotel reviews as either truthful or deceptive, and either positive or negative 
* Used word tokens as features for classification
* Approached the problem as two binary classification problems (Truthful/Deceptive) and (Positive/Negative)

###Data
* The training data is present in the ```IO Files``` folder. The training text file is called ```train-text.txt``` and contains hotel reviews separated by new lines. The corressponding labels are present in the file ```train-labels.txt```
* The test data is present in the file ```test-text.txt```. This is the same as training data (Change the file according to the requirement)

###Programs
* The ```main.py``` file is the entrypoint of the application. This file calls functions from ```nblearn3.py``` and ```nbclassify3.py``` files
* The learning functions produce a ```nbmodel.txt``` file that contains the model (Tokens, their probabilites and prior probabilities)
* The final output (labels corressponding to the test file) is written to a file called ```nboutput.txt``` 

