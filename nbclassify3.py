import math

def read_model(model_filename):
    priors = []
    word_probabilities = dict()
    file_contents = []
    index_list = []

    list_sep = open(model_filename).read().split('\n')

    for token in list_sep:
        tl = token.split(' ')
        file_contents.append(tl)

    for i in range(len(file_contents)):
        if file_contents[i][0] == '$':
            index_list.append(i)

    for i in range(index_list[0]):
        word_probabilities[file_contents[i][0]] = [file_contents[i][1], file_contents[i][2],
                                                   file_contents[index_list[0] + 1 + i][1],
                                                   file_contents[index_list[0] + 1 + i][2]]

    for i in range(index_list[1] + 1, len(file_contents)):
        priors.append(file_contents[i])

    return word_probabilities, priors


def find_probability(token, classification1, dict1):
    if token not in dict1:
        return 1.0
    if classification1 == 'truthful':
        return dict1[token][0]
    elif classification1 == 'deceptive':
        return dict1[token][1]

    elif classification1 == 'positive':
        return dict1[token][2]
    elif classification1 == 'negative':
        return dict1[token][3]


def compute_probabilities(word_probabilities, priors, final_list):
    l = len(final_list)
    w = 4
    review_classification = [[0 for x in range(w)] for y in range(l)]
    m = 0
    n = 0
    o = 0
    p = 0
    somevar = 0
    for list in final_list:
        for word in list:
            m += math.log(float(find_probability(word, 'truthful', word_probabilities)), 10)
            n += math.log(float(find_probability(word, 'deceptive', word_probabilities)), 10)
            o += math.log(float(find_probability(word, 'positive', word_probabilities)), 10)
            p += math.log(float(find_probability(word, 'negative', word_probabilities)), 10)
        review_classification[somevar][0] = m + math.log(float(priors[0][2]), 10)
        review_classification[somevar][1] = n + math.log(float(priors[1][2]), 10)
        review_classification[somevar][2] = o + math.log(float(priors[2][2]), 10)
        review_classification[somevar][3] = p + math.log(float(priors[3][2]), 10)
        m = 0
        n = 0
        o = 0
        p = 0
        somevar += 1

    return review_classification


def read_test_file(filename):
    character_seq = []
    list_fin = [list.rstrip('\n') for list in open(filename,'r')]
    for i in range(len(list_fin)):
        character_seq.append(list_fin[i][:20])
        list_fin[i]=list_fin[i].replace(list_fin[i][:21],'')
    return character_seq, list_fin


# writing output to a text file
def write_to_file(review_classification, filename, character_seq):
    out_file = open(filename, 'w')
    for i in range(len(review_classification)):
        out_file.write(character_seq[i])
        out_file.write(' ')
        if review_classification[i][0] > review_classification[i][1]:
            out_file.write('truthful'.strip())
        else:
            out_file.write('deceptive'.strip())

        out_file.write(' ')

        if review_classification[i][2] > review_classification[i][3]:
            out_file.write('positive'.strip())
        else:
            out_file.write('negative'.strip())

        out_file.write('\n')
