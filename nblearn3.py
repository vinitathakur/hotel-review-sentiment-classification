import re


def read_data(input_filename):
    paragraph_review_list = [list.rstrip('\n') for list in open(input_filename,'r')]
    for i in range(len(paragraph_review_list)):
        paragraph_review_list[i] = paragraph_review_list[i].replace(paragraph_review_list[i][:21], '')
    return paragraph_review_list


def cleaning(review_list, punctuation_list, stopword_list):
    training_token_list = []
    #convert all elements to lowercase, remove stopwords, remove punctuations, filter null values
    for review in review_list:
        intermediate = review.split(' ')
        intermediate = [element.lower() for element in intermediate]
        for k in range(len(intermediate)):
            intermediate[k] = ''.join(ch for ch in intermediate[k] if ch not in punctuation_list)
        intermediate = list(set(intermediate) - set(stopword_list))
        intermediate = list(filter(None, intermediate))
        training_token_list.append(intermediate)
    #defining a regular expression to exclude numbers
    pattern = re.compile('[a-z]*')

    for token_list in training_token_list:
        for token in token_list:
            if pattern.fullmatch(token) == None:
                token_list.remove(token)

    return training_token_list

def create_label_lists(labelData):
#create label lists for two binary classifiers
    list_of_labels = [list.rstrip('\n') for list in open(labelData,'r')]

    for i in range(len(list_of_labels)):
        list_of_labels[i] = list_of_labels[i].replace(list_of_labels[i][:21],'')

    classifier1_labels = []
    classifier2_labels = []

    for labels in list_of_labels:
        temp = labels.split(" ")
        classifier1_labels.append(temp[0])
        classifier2_labels.append(temp[1])

    return classifier1_labels, classifier2_labels


def calculate_priors(classifier1_labels, classifier2_labels):
    g = 0
    pos1 = 0
    neg1 = 0
    tru1 = 0
    dec1 = 0
    for label in classifier1_labels:
        if label == "truthful":
            tru1+=1
        else:
            dec1+=1
        if classifier2_labels[g] == "positive":
            pos1+=1
        else:
            neg1+=1
        g += 1

    prior = {}
    prior['p'] = pos1/float(pos1+neg1)
    prior['n'] = neg1/float(pos1+neg1)
    prior['t'] = tru1/float(pos1+neg1)
    prior['d'] = dec1/float(pos1+neg1)

    return prior


def calculate_probabilities(training_token_list, classifier1_labels, classifier2_labels ):
    i = -1
    word_probability_dict = dict()
    for token_list in training_token_list:
        i += 1
        for token in token_list:
            if token in word_probability_dict:
                if classifier1_labels[i] == "truthful":
                    word_probability_dict[token][0] += 1
                else:
                    word_probability_dict[token][1] += 1
                if classifier2_labels[i] == "positive":
                    word_probability_dict[token][2] += 1
                else:
                    word_probability_dict[token][3] += 1

            else:
                word_probability_dict[token] = [1, 1, 1, 1]
                if classifier1_labels[i] == "truthful":
                    word_probability_dict[token][0] += 1
                else:
                    word_probability_dict[token][1] += 1

                if classifier2_labels[i] == "positive":
                    word_probability_dict[token][2] += 1
                else:
                    word_probability_dict[token][3] += 1

    pos = 0
    neg = 0
    tru = 0
    dec = 0

    for key, value in word_probability_dict.items():
        tru += value[0]
        dec += value[1]
        pos += value[2]
        neg += value[3]

    for key in word_probability_dict.keys():
        word_probability_dict[key][0] /= float(tru)
        word_probability_dict[key][1] /= float(dec)
        word_probability_dict[key][2] /= float(pos)
        word_probability_dict[key][3] /= float(neg)

    return word_probability_dict


def write_to_file(word_probability_dict, prior, filename):
    ff = open(filename, 'w')

    #model1 truthful deceptive
    for word in word_probability_dict:
        ff.write(word)
        ff.write(' ')
        ff.write(str(word_probability_dict[word][0]))
        ff.write(' ')
        ff.write(str(word_probability_dict[word][1]))
        ff.write('\n')
    ff.write('$')
    ff.write('\n')

    #model 2 positive negative
    for word in word_probability_dict:
        ff.write(word)
        ff.write(' ')
        ff.write(str(word_probability_dict[word][2]))
        ff.write(' ')
        ff.write(str(word_probability_dict[word][3]))
        ff.write('\n')
    ff.write('$')
    ff.write('\n')

    #prior probabilities
    ff.write('prior truthful')
    ff.write(' ')
    ff.write(str(prior['t']))
    ff.write('\n')
    ff.write('prior deceptive')
    ff.write(' ')
    ff.write(str(prior['d']))
    ff.write('\n')
    ff.write('prior positive')
    ff.write(' ')
    ff.write(str(prior['p']))
    ff.write('\n')
    ff.write('prior negative')
    ff.write(' ')
    ff.write(str(prior['n']))
    ff.write('\n')
